from django.forms import ModelForm
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import *

class JenisForm(ModelForm):
    class Meta:
        model = Jenis
        fields = '__all__'
        
        widgets = {
			'nama' : forms.TextInput({'class':'form-control'}),
		}

class TanamanForm(ModelForm):
    class Meta:
        model = Tanaman
        fields = '__all__'
        
        widgets = {
			'nama' : forms.TextInput({'class':'form-control'}),
			'harga_beli' : forms.NumberInput({'class':'form-control'}),
            'harga_jual' : forms.NumberInput({'class':'form-control'}),
		}

class AksesorisForm(ModelForm):
    class Meta:
        model = Aksesoris
        fields = '__all__'
        
        widgets = {
			'nama' : forms.TextInput({'class':'form-control'}),
			'harga' : forms.NumberInput({'class':'form-control'}),
            'stok' : forms.NumberInput({'class':'form-control'}),
		}

class UserForm(ModelForm):
    # first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')
        
        widgets = {
			'first_name' : forms.TextInput({'class':'form-control'}),
			'last_name' : forms.TextInput({'class':'form-control'}),
            'username' : forms.TextInput({'class':'form-control'}),
            'email' : forms.EmailInput({'class':'form-control'}),
		}

class CreateUserForm(UserCreationForm):
    # first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    # email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2',)
        
        widgets = {
			'first_name' : forms.TextInput({'class':'form-control'}),
			'last_name' : forms.TextInput({'class':'form-control'}),
            'username' : forms.TextInput({'class':'form-control'}),
            'email' : forms.EmailInput({'class':'form-control'}),
            'password1' : forms.TextInput({'class':'form-control'}),
            'password2' : forms.TextInput({'class':'form-control'}),
		}