from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.


class Jenis(models.Model):
    nama = models.CharField(max_length=50)
    
    def __str__(self):
        """String for representing the Model object."""
        return self.nama

class Tanaman(models.Model):
    nama = models.CharField(max_length=100)
    jenis = models.ForeignKey('Jenis', on_delete=models.SET_NULL, null=True)
    gambar = models.ImageField(upload_to='gambar/', null=True)
    deskripsi = models.TextField(max_length=1000)
    harga_beli = models.IntegerField('Harga Beli (Rp)')
    harga_jual = models.IntegerField('Harga Jual (Rp)')
    
    def __str__(self):
        """String for representing the Model object."""
        return self.nama

    def get_absolute_url(self):
        """Returns the url to access a detail record for this book."""
        return reverse('tanaman-detail', args=[str(self.id)])

class Aksesoris(models.Model):
    nama = models.CharField(max_length=100)
    harga = models.IntegerField('Harga (Rp)')
    stok = models.IntegerField()
    
    def __str__(self):
        """String for representing the Model object."""
        return self.nama