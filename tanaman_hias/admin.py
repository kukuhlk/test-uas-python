from django.contrib import admin
from .models import Jenis,Tanaman,Aksesoris
# Register your models here.

@admin.register(Jenis)
class JenisAdmin(admin.ModelAdmin): #new
	list_display = ['nama']

@admin.register(Tanaman)
class TanamanAdmin(admin.ModelAdmin): #new
    list_display = ['nama','jenis','gambar','harga_beli','harga_jual']
    list_filter = ['jenis']

@admin.register(Aksesoris)
class AksesorisAdmin(admin.ModelAdmin): #new
    list_display = ['nama','harga','stok']
