from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.admin.views.decorators import staff_member_required
from .models import *
from .forms import *

def loginPage(request):
	if request.user.is_authenticated:
		return redirect('/dashboard')
	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			password =request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('/dashboard')
			else:
				messages.error(request, 'Username atau password salah')

		context = {}
		return render(request, 'auth/sign-in.html', context)

@login_required(login_url='login')
def logoutPage(request):
	logout(request)
	return redirect('/login')

@login_required(login_url='login')
def dashboard(request) :
    tanamans = Tanaman.objects.all().count()
    jeniss = Jenis.objects.all().count()
    aksesoriss = Aksesoris.objects.all().count()
    pegawaiss = User.objects.filter(is_staff=False).count()
    context = {
        'total_tanaman':tanamans,
        'total_jenis':jeniss,
        'total_aksesoris':aksesoriss,
        'total_pegawai' :pegawaiss,
        'request':request,
    }
    return render(request, 'dashboard.html',context)

# Kelola Tanaman
@login_required(login_url='login')
def tanaman(request) :
    tanamans = Tanaman.objects.all()
    return render(request, 'tanaman_hias/data_tanaman_hias.html',{'tanamans':tanamans,'request':request})
@login_required(login_url='login')
def add_tanaman(request) :
    if request.POST:
        form = TanamanForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            tanaman = form.cleaned_data.get('nama')
            messages.success(request, 'Berhasil menambah data tanaman : ' + tanaman)
            return redirect('/tanaman')
    else:
        form = TanamanForm()
        judul = "Tambah Tanaman"
        context = {
			'title': judul,
			'form': form,
		}
        return render(request, 'tanaman_hias/tambah_data_tanaman_hias.html', context)
@login_required(login_url='login')
def edit_tanaman(request,pk) :
    tanaman = Tanaman.objects.get(id=pk)
    if request.POST:
        form = TanamanForm(request.POST, request.FILES, instance=tanaman)
        if form.is_valid():
            form.save()
            tanaman = form.cleaned_data.get('nama')
            messages.success(request, 'Berhasil mengubah data tanaman : ' + tanaman)
            return redirect('/tanaman')
    else:
        form = TanamanForm(instance=tanaman)
        judul = "Ubah Tanaman"
        context = {
            'form': form,
            'tanaman': tanaman,
            'title': judul,
        }
        return render(request, 'tanaman_hias/edit_data_tanaman_hias.html',context)
@login_required(login_url='login')
def delete_tanaman(request,pk) :
    tanaman = Tanaman.objects.filter(id=pk)
    tanaman.delete()
    
    messages.success(request, 'Berhasil menghapus data tanaman')
    return redirect('/tanaman')
    
# Kelola Jenis
@login_required(login_url='login')
def jenis(request) :
    jeniss = Jenis.objects.all()
    return render(request, 'jenis/data_jenis.html',{'jeniss':jeniss,'request':request})
@login_required(login_url='login')
def add_jenis(request) :
    if request.POST:
        form = JenisForm(request.POST)
        if form.is_valid():
            form.save()
            jenis = form.cleaned_data.get('nama')
            messages.success(request, 'Berhasil menambah data jenis : ' + jenis)
            return redirect('/jenis')
    else:
        form = JenisForm()
        judul = "Tambah Jenis"
        context = {
			'title': judul,
			'form': form,
		}
        return render(request, 'jenis/tambah_data_jenis.html', context)
@login_required(login_url='login')
def edit_jenis(request,pk) :
    jenis = Jenis.objects.get(id=pk)
    if request.POST:
        form = JenisForm(request.POST, instance=jenis)
        if form.is_valid():
            form.save()
            jenis = form.cleaned_data.get('nama')
            messages.success(request, 'Berhasil mengubah data jenis : ' + jenis)
            return redirect('/jenis')
    else:
        form = JenisForm(instance=jenis)
        judul = "Ubah Jenis"
        context = {
            'form': form,
            'jenis': jenis,
            'title': judul,
        }
        return render(request, 'jenis/edit_data_jenis.html',context)
@login_required(login_url='login')
def delete_jenis(request,pk) :
    jenis = Jenis.objects.filter(id=pk)
    jenis.delete()
    
    messages.success(request, 'Berhasil menghapus data jenis')
    return redirect('/jenis')

# Kelola Aksesoris
@login_required(login_url='login')
def aksesoris(request) :
    aksesoriss = Aksesoris.objects.all()
    return render(request, 'aksesoris/data_aksesoris.html',{'aksesoriss':aksesoriss,'request':request})
@login_required(login_url='login')
def add_aksesoris(request) :
    if request.POST:
        form = AksesorisForm(request.POST)
        if form.is_valid():
            form.save()
            aksesoris = form.cleaned_data.get('nama')
            messages.success(request, 'Berhasil menambah data aksesoris : ' + aksesoris)
            return redirect('/aksesoris')
    else:
        form = AksesorisForm()
        judul = "Tambah Aksesoris"
        context = {
			'title': judul,
			'form': form,
		}
        return render(request, 'aksesoris/tambah_data_aksesoris.html', context)
@login_required(login_url='login')
def edit_aksesoris(request,pk) :
    aksesoris = Aksesoris.objects.get(id=pk)
    if request.POST:
        form = AksesorisForm(request.POST, instance=aksesoris)
        if form.is_valid():
            form.save()
            aksesoris = form.cleaned_data.get('nama')
            messages.success(request, 'Berhasil mengubah data aksesoris : ' + aksesoris)
            return redirect('/aksesoris')
    else:
        form = AksesorisForm(instance=aksesoris)
        judul = "Ubah Aksesoris"
        context = {
            'form': form,
            'aksesoris': aksesoris,
            'title': judul,
        }
        return render(request, 'aksesoris/edit_data_aksesoris.html',context)
@login_required(login_url='login')
def delete_aksesoris(request,pk) :
    aksesoris = Aksesoris.objects.filter(id=pk)
    aksesoris.delete()
    
    messages.success(request, 'Berhasil menghapus data aksesoris')
    return redirect('/aksesoris')

# Kelola Pegawai
@login_required(login_url='login')
@staff_member_required
def pegawai(request) :
    pegawais = User.objects.filter(is_staff=False)
    return render(request, 'pegawai/data_pegawai.html',{'pegawais':pegawais,'request':request})
@login_required(login_url='login')
@staff_member_required
def add_pegawai(request) :
    if request.POST:
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            pegawai = form.cleaned_data.get('username')
            messages.success(request, 'Berhasil menambah data pegawai : ' + pegawai)
            return redirect('/pegawai')
    else:
        form = CreateUserForm()
        judul = "Tambah User"
        context = {
			'title': judul,
			'form': form,
		}
        return render(request, 'pegawai/tambah_data_pegawai.html', context)
@login_required(login_url='login')
@staff_member_required
def edit_pegawai(request,pk) :
    pegawai = User.objects.get(id=pk)
    if request.POST:
        form = UserForm(request.POST, request.FILES, instance=pegawai)
        if form.is_valid():
            user = form.save()
            user.set_password('password')
            user.save()
            pegawai = form.cleaned_data.get('username')
            messages.success(request, 'Berhasil mengubah data pegawai : ' + pegawai)
            return redirect('/pegawai')
    else:
        form = UserForm(instance=pegawai)
        judul = "Ubah User"
        context = {
            'form': form,
            'pegawai': pegawai,
            'title': judul,
        }
        return render(request, 'pegawai/edit_data_pegawai.html',context)
@login_required(login_url='login')
@staff_member_required
def delete_pegawai(request,pk) :
    pegawai = User.objects.filter(id=pk)
    pegawai.delete()
    
    messages.success(request, 'Berhasil menghapus data pegawai')
    return redirect('/pegawai')
# Create your views here.
