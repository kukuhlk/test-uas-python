"""sistem_informasi_tanaman_hias URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from tanaman_hias.views import *
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import RedirectView

urlpatterns = [
    # Halaman admin
    path('admin/', admin.site.urls),
    
    # Halaman Autentikasi
    path('login/', loginPage, name="login"),  
    path('logout/', logoutPage, name="logout"),
    
    # Halaman Utama
    path('', RedirectView.as_view(url='dashboard',permanent=True)),
    path('dashboard/', dashboard, name='dashboard'),
    
    # Halaman Kelola Tanaman Hias
    path('tanaman/', tanaman, name='tanaman'),
    path('tanaman-add/', add_tanaman, name='add_tanaman'),
    path('tanaman/edit/<int:pk>', edit_tanaman, name='edit_tanaman'),
    path('tanaman/delete/<int:pk>', delete_tanaman, name='delete_tanaman'),
    
    # Halaman Kelola Jenis Tanaman
    path('jenis/', jenis, name='jenis'),
    path('jenis-add/', add_jenis, name='add_jenis'),
    path('jenis/edit/<int:pk>', edit_jenis, name='edit_jenis'),
    path('jenis/delete/<int:pk>', delete_jenis, name='delete_jenis'),
    
    # Halaman Kelola Aksesoris
    path('aksesoris/', aksesoris, name='aksesoris'),
    path('aksesoris-add/', add_aksesoris, name='add_aksesoris'),
    path('aksesoris/edit/<int:pk>', edit_aksesoris, name='edit_aksesoris'),
    path('aksesoris/delete/<int:pk>', delete_aksesoris, name='delete_aksesoris'),
    
    # Halaman Kelola Pegawai
    path('pegawai/', pegawai, name='pegawai'),
    path('pegawai-add/', add_pegawai, name='add_pegawai'),
    path('pegawai/edit/<int:pk>', edit_pegawai, name='edit_pegawai'),
    path('pegawai/delete/<int:pk>', delete_pegawai, name='delete_pegawai'),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)